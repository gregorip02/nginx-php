FROM nginx:1.17-alpine

# Add templates files
COPY default.conf /etc/nginx/conf.d/default.conf.tmp
COPY docker-entrypoint.sh /usr/local/bin/ 

RUN ln -s /usr/local/bin/docker-entrypoint.sh /
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

# Execute entrypoint script
ENTRYPOINT ["docker-entrypoint.sh"]

