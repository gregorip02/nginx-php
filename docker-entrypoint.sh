#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset

NGINX_CONFIG_FILE="/etc/nginx/conf.d/default.conf"

envsubst '$$NGINX_HOST $$NGINX_FASTCGI $$NGINX_ROOT' < "${NGINX_CONFIG_FILE}.tmp" > "${NGINX_CONFIG_FILE}" 

nginx -g "daemon off;"

